#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#include <cstdint>
#include <dlfcn.h>
#endif

#include <link.h>

#include <sstream>
#include <iostream>
#include <string_view>
#include <regex>

static std::regex re_libc("(.*/)?libc\\.so(\\.[\\d.]+)?");
static std::regex re_libdl("(.*/)?libdl\\.so(\\.[\\d.]+)?");
static std::regex re_ldso("(.*/)?ld[^.]*\\.so(\\.[\\d.]+)?");
static std::regex re_vdso("linux-vdso\\.so(\\.[\\d.]+)?");
static std::string_view basen(std::string_view path) {
  auto slash = path.rfind('/');
  auto result = slash != std::string_view::npos ? path.substr(slash) : path;
  if(std::regex_match(result.begin(), result.end(), re_libc))
    return "[libc]";
  if(std::regex_match(result.begin(), result.end(), re_libdl))
    return "[libdl]";
  if(std::regex_match(result.begin(), result.end(), re_ldso))
    return "[ld.so]";
  if(std::regex_match(result.begin(), result.end(), re_vdso))
    return "[vDSO]";
  if(result == "")
    return "/[exe]";
  return result;
}
static std::string_view la_act(unsigned int flag) {
  switch(flag) {
  case LA_ACT_ADD: return "LA_ACT_ADD";
  case LA_ACT_DELETE: return "LA_ACT_DELETE";
  case LA_ACT_CONSISTENT: return "LA_ACT_CONSISTENT";
  default: return "???";
  }
}
static std::string la_symb(unsigned int flag) {
  if(flag == 0) return "0";

  std::ostringstream ss;
  bool first = true;
  for(auto [mask, name]: {std::make_pair(LA_SYMB_DLSYM, "LA_SYMB_DLSYM"), std::make_pair(LA_SYMB_ALTVALUE, "LA_SYMB_ALTVALUE")}) {
    if((mask & flag) != 0) {
      if(!first) ss << "|";
      ss << name;
      flag &= ~(unsigned int)mask;
      first = false;
    }
  }
  // if(flag != 0) {
  //   if(!first) ss << "|";
  //   ss << "0x" << std::hex << flag;
  // }
  if(first) return "0";
  return ss.str();
}
static Lmid_t fetchlmid(struct link_map* map) {
  Lmid_t lmid;
  if(dlinfo(map, RTLD_DI_LMID, (void*)&lmid) != 0) {
    std::abort();
  }
  return lmid;
}

namespace {
struct Map {
  struct link_map* map;
  explicit Map(struct link_map* map) : map(map) {}
  explicit Map(uintptr_t map) : map((struct link_map*)map) {}
};
}
static std::ostream& operator<<(std::ostream& os, const Map& map) {
  return os << "<" << basen(map.map->l_name) << " : " << fetchlmid(map.map) << ">";
}

static bool last_was_objsearch = false;

unsigned int la_version(unsigned int v) {
  return LAV_CURRENT;
}

char* la_objsearch(const char* name, uintptr_t* cookie, unsigned int flag) {
  if(!last_was_objsearch) {
    auto bn = basen(name);
    if(bn[0] != '[') {
      std::cout << "[audit] la_objsearch(" << basen(name) << ", ...)" << std::endl;
    }
    last_was_objsearch = true;
  }
  return (char*)name;
}

void la_activity(uintptr_t* cookie, unsigned int flag) {
  last_was_objsearch = false;
  std::cout << "[audit] la_activity(" << la_act(flag) << ")" << std::endl;
}

unsigned int la_objopen(struct link_map *map, Lmid_t lmid, uintptr_t *cookie) {
  last_was_objsearch = false;
  std::cout << "[audit] la_objopen(" << basen(map->l_name) << ", " << lmid << ")" << std::endl;
  return LA_FLG_BINDFROM | LA_FLG_BINDTO;
}

unsigned int la_objclose(uintptr_t* cookie) {
  last_was_objsearch = false;
  std::cout << "[audit] la_objclose(" << Map(*cookie) << ")" << std::endl;
  return 0;
}

void la_preinit(uintptr_t* cookie) {
  last_was_objsearch = false;
  std::cout << "[audit] la_preinit(" << Map(*cookie) << ")" << std::endl;
}

uintptr_t la_symbind64(Elf64_Sym *sym, unsigned int ndx, uintptr_t *refcook, uintptr_t *defcook, unsigned int *flags, const char *symname) {
  last_was_objsearch = false;
  auto refname = basen(((struct link_map*)*refcook)->l_name);
  if(refname[0] != '[' && symname[0] != '_') {
    std::cout << "[audit] la_symbind(" << Map(*refcook) << ", " << Map(*defcook) << ", " << la_symb(*flags) << ", " << symname << ")" << std::endl;
  }
  return sym->st_value;
}
