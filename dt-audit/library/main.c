#include <dlfcn.h>

extern void a();

int main() {
  a();
  void* h = dlopen("libb.so", RTLD_NOW);
  void (*b)() = dlsym(h, "b");
  b();
  dlclose(h);
  return 0;
}
