# Link-time Support for LD_AUDIT #

This directory tests the Glibc support for `DT_AUDIT` and `DT_DEPAUDIT`, two entries in the dynamic
section of an executable that can be used to load auditors without setting environment variables.

## Results

Glibc 2.35 has full support for `DT_AUDIT` and `DT_DEPAUDIT`:
 - Linking an executable with `-Wl,--audit=<auditor>` produces a `DT_AUDIT` entry and loads the
   given auditor at runtime. `<auditor>` can be a full path, or a library name which will be
   searched using the executable's `DT_R[UN]PATH` entries.
 - Shared libraries linked with `-Wl,--audit=<auditor>` will produce a `DT_DEPAUDIT` entry when
   linked into a dynamic executable, which will also load the given auditor at runtime. If
   `<auditor>` is not a full path in this case it will be searched using the *executable's*
   `DT_R[UN]PATH` entries.
 - All notifications when using `DT_[DEP]AUDIT` are identical to when `LD_AUDIT` is used.
