#!/usr/bin/env python3

import subprocess
import sys
import difflib

canonical = sys.argv[1]
args = sys.argv[2:]

with open(canonical, 'r') as f:
  canonical = f.readlines()

proc = subprocess.run(args, capture_output=True, text=True)
sys.stderr.write(proc.stderr)
if proc.returncode != 0:
  sys.exit(proc.returncode)
result = proc.stdout.splitlines(keepends=True)

if canonical != result:
  print(''.join(difflib.ndiff(canonical, result)), end='')
  sys.exit(1)
