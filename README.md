# LD_AUDIT Poweruser Tests

Tests for the latest and greatest features in LD_AUDIT, for us powerusers that need the good stuff.

 - [Symbol Wrapping](symbol-wrapping/)
 - [Link-time Support for LD_AUDIT](dt-audit/)
