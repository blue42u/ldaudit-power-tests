#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "common.hpp"

#include <dlfcn.h>
#include <iostream>

int main(int argc, const char** argv) {
  auto expected = parse(argc, argv);
  TEST([](const char* s) -> str_t { return victim(s); }, expected);

  void* h = dlmopen(LM_ID_NEWLM, "libvictim.so", RTLD_NOW | RTLD_LOCAL);
  if(h == nullptr) {
    std::cerr << "Unable to dlopen libvictim.so\n";
    return 1;
  }
  pfn_victim_t pVictim = (pfn_victim_t)dlsym(h, "victim");
  if(pVictim == nullptr) {
    std::cerr << "libvictim.so does not contain victim\n";
    return 1;
  }
  bool ok = TEST(pVictim, expected);
  dlclose(h);
  return ok ? 0 : 1;
}
