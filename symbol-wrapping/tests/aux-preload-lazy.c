#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "victim.h"

#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

static pfn_victim_t real_victim = NULL;

str_t victim(const char* msg) {
  if(real_victim == NULL) {
    real_victim = dlsym(RTLD_NEXT, "victim");
    if(real_victim == NULL)
      abort();
  }
  char buf[2048];
  snprintf(buf, sizeof buf, "%s -> lazy-preload()", msg);
  return real_victim(buf);
}
