#include "common.hpp"

#include <dlfcn.h>
#include <iostream>

int main(int argc, const char** argv) {
  auto expected = parse(argc, argv);
  bool ok = true;
  for(int i = 0; i < 4; i++) {
    void* h = dlopen("libvictim.so", RTLD_NOW | RTLD_LOCAL);
    if(h == nullptr) {
      std::cerr << "Unable to dlopen libvictim.so\n";
      return 1;
    }
    pfn_victim_t pVictim = (pfn_victim_t)dlsym(h, "victim");
    if(pVictim == nullptr) {
      std::cerr << "libvictim.so does not contain victim\n";
      return 1;
    }
    ok = ok && TEST(pVictim, expected);
    dlclose(h);
  }
  return ok ? 0 : 1;
}
