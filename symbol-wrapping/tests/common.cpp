#include "common.hpp"

#include <iostream>

std::string parse(int argc, const char** argv) {
  if(argc != 2) {
    std::cerr << "Expected exactly 1 argument!\n";
    std::exit(2);
  }
  return argv[1];
}

std::pair<std::string, std::string> parse2(int argc, const char** argv) {
  if(argc != 3) {
    std::cerr << "Expected exactly 2 arguments!\n";
    std::exit(2);
  }
  return {argv[1], argv[2]};
}

std::string call(pfn_victim_t fun, const std::string& arg) {
  str_t out = fun(arg.c_str());
  std::string result = out.str;
  out.free(out.str);
  return result;
}

bool _test(pfn_victim_t victim, std::string_view expected_path, const char* where,
            const std::string& slug) {
  std::string result = call(victim, slug);
  std::string expected;
  auto repl = expected_path.find("%%");
  if(repl != std::string::npos) {
    expected = expected_path;
    expected.replace(repl, 2, slug);
  } else {
    expected = slug;
    expected += expected_path;
  }
  if(result != expected) {
    std::cerr << where << ": Expected \"" << expected << "\", got \"" << result << "\"\n";
    return false;
  }
  std::cerr << where << ": " << result << "\n";
  return true;
}
