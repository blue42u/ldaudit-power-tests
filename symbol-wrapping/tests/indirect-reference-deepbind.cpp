#include "common.hpp"
#include <dlfcn.h>

int main(int argc, const char** argv) {
  auto expected = parse(argc, argv);
  void* h = dlopen("libindirect-reference-b.so", RTLD_LAZY | RTLD_GLOBAL | RTLD_DEEPBIND);
  pfn_victim_t victim = (pfn_victim_t)dlsym(h, "call_victim");
  bool ok = TEST(victim, expected);
  dlclose(h);
  return ok ? 0 : 1;
}
