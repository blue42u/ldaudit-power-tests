#include "common.hpp"

int main(int argc, const char** argv) {
  auto expected = parse(argc, argv);
  return TEST([](const char* s) -> str_t { return victim(s); }, expected) ? 0 : 1;
}
