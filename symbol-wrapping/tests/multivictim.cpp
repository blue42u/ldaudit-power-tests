#include "common.hpp"

#include <dlfcn.h>
#include <iostream>

int main(int argc, const char** argv) {
  auto [expected, expectedNot] = parse2(argc, argv);

  void* h = dlopen("libvictim.so", RTLD_NOW);
  if(h == nullptr) {
    std::cerr << "Unable to dlopen libvictim.so\n";
    return 1;
  }
  pfn_victim_t pVictim = (pfn_victim_t)dlsym(h, "victim");
  if(pVictim == nullptr) {
    std::cerr << "libvictim.so does not contain victim\n";
    return 1;
  }

  void* hNot = dlopen("libnotvictim.so", RTLD_NOW);
  if(hNot == nullptr) {
    std::cerr << "Unable to dlopen libnotvictim.so\n";
    return 1;
  }
  pfn_victim_t pNotVictim = (pfn_victim_t)dlsym(hNot, "victim");
  if(pNotVictim == nullptr) {
    std::cerr << "libnotvictim.so does not contain victim\n";
    return 1;
  }

  bool ok = TEST(pVictim, expected);
  bool ok2 = TEST(pNotVictim, expectedNot);

  dlclose(h);
  dlclose(hNot);
  return (ok && ok2) ? 0 : 1;
}
