#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "victim.h"

#include <link.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

unsigned int la_version(unsigned int v) {
  return LAV_CURRENT;
}

static str_t new_victim(const char* msg) {
  char buf[2048];
  snprintf(buf, sizeof buf, "%s -> overload()", msg);
  return (str_t){strdup(buf), &free};
}

unsigned int la_objopen(struct link_map *map, Lmid_t lmid, uintptr_t *cookie) {
  return LA_FLG_BINDFROM | LA_FLG_BINDTO;
}

uintptr_t la_symbind64(Elf64_Sym *sym, unsigned int ndx, uintptr_t *refcook, uintptr_t *defcook,
                       unsigned int *flags, const char *symname) {
  if(strcmp(symname, "victim") == 0) {
    return (uintptr_t)(pfn_victim_t)new_victim;
  }
  return sym->st_value;
}
