#include "victim.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

str_t victim(const char* msg) {
  char buf[2048];
  snprintf(buf, sizeof buf, "%s -> overload()", msg);
  return (str_t){strdup(buf), &free};
}
