#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "victim.h"

#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

static pfn_victim_t real_victim = NULL;

__attribute__((constructor))
static void init() {
  real_victim = dlsym(RTLD_NEXT, "victim");
  if(real_victim == NULL)
    abort();
}

str_t victim(const char* msg) {
  char buf[2048];
  snprintf(buf, sizeof buf, "PRE %s -> eager-preload()", msg);
  return real_victim(buf);
}
