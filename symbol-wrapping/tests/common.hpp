#ifndef _H_TESTS_COMMON_
#define _H_TESTS_COMMON_

#include "victim.h"

#include <string>

#define STR(x) #x
#define STRINGIFY(x) STR(x)

// Helpers to parse argc/argv
std::string parse(int argc, const char** argv);
std::pair<std::string, std::string> parse2(int argc, const char** argv);

// Wrapper to call a victim function with C++ semantics
std::string call(pfn_victim_t victim, const std::string& arg);

bool _test(pfn_victim_t victim, std::string_view expected_prefix, const char* where,
            const std::string& slug);

// Test that the given victim fuction is correctly wrapped
#define TEST(VICTIM, PREFIX) \
  _test((VICTIM), (PREFIX), __FILE__ ":" STRINGIFY(__LINE__), "[:" STRINGIFY(__LINE__) "]")

#endif  // _H_TESTS_COMMON_
