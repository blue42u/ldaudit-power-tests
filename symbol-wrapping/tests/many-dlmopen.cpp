#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "common.hpp"

#include <dlfcn.h>
#include <array>
#include <iostream>

int main(int argc, const char** argv) {
  auto expected = parse(argc, argv);
  TEST([](const char* s) -> str_t { return victim(s); }, expected);

  std::array<void*, 10> handles;
  bool ok = true;
  for(void*& h: handles) {
    h = dlmopen(LM_ID_NEWLM, "libvictim.so", RTLD_NOW | RTLD_LOCAL);
    if(h == nullptr) {
      std::cerr << "Unable to dlopen libvictim.so\n";
      return 1;
    }
    pfn_victim_t pVictim = (pfn_victim_t)dlsym(h, "victim");
    if(pVictim == nullptr) {
      std::cerr << "libvictim.so does not contain victim\n";
      return 1;
    }
    bool myok = TEST(pVictim, expected);
    ok = ok && myok;
  }
  for(void*& h: handles) {
    dlclose(h);
  }
  return ok ? 0 : 1;
}
