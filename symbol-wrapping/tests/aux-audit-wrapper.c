#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "victim.h"

#include <link.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

unsigned int la_version(unsigned int v) {
  return LAV_CURRENT;
}

static pfn_victim_t real_victim = NULL;
static str_t new_victim(const char* msg) {
  char buf[2048];
  snprintf(buf, sizeof buf, "%s -> audit-wrapper()", msg);
  return real_victim(buf);
}

unsigned int la_objopen(struct link_map *map, Lmid_t lmid, uintptr_t *cookie) {
  return lmid == LM_ID_BASE ? LA_FLG_BINDFROM | LA_FLG_BINDTO : 0;
}

uintptr_t la_symbind64(Elf64_Sym *sym, unsigned int ndx, uintptr_t *refcook, uintptr_t *defcook,
                       unsigned int *flags, const char *symname) {
  if(strcmp(symname, "victim") == 0) {
    fprintf(stderr, "before: %p  new: %p\n", real_victim, (void*)sym->st_value);
    real_victim = (pfn_victim_t)sym->st_value;
    return (uintptr_t)new_victim;
  }
  return sym->st_value;
}
