#include "victim.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

str_t victim(const char* in) {
  char buf[2048];
  snprintf(buf, sizeof buf, "%s -> victim()", in);
  return (str_t){strdup(buf), &free};
}
