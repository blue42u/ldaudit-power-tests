#ifndef _H_LIBVICTIM_
#define _H_LIBVICTIM_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
  char* str;
  void (*free)(void*);
} str_t;

// Identical to strdup, except also returns the appropriate free() function
str_t victim(const char*);

typedef str_t (*pfn_victim_t)(const char*);

#ifdef __cplusplus
}  // extern "C"
#endif

#endif  // _H_LIBVICTIM_
