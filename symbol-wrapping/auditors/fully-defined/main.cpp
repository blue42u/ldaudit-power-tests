#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <link.h>

#include "victim.h"

#include <string_view>
#include <mutex>
#include <iostream>
#include <array>
#include <regex>

unsigned int la_version(unsigned int) {
  return LAV_CURRENT;
}

// This auditor supports up to 3 linkage namespaces. This value can be increased in practice but for
// compatibility with the test suite we limit it here.
static std::mutex wrap_lock;
static std::array<pfn_victim_t, 3> victims = {nullptr, nullptr, nullptr};
template<int Idx>
static str_t wrapped_victim(const char* msg) {
  std::string m = msg;
  m += " -> auditor()";
  return victims.at(Idx)(m.c_str());
}
static std::array<pfn_victim_t, victims.size()> wrappers = {
  wrapped_victim<0>, wrapped_victim<1>, wrapped_victim<2>
};

static std::regex libvictim_re(".*/libvictim\\.so[^/]*$", std::regex::ECMAScript | std::regex::optimize | std::regex::nosubs);
unsigned int la_objopen(struct link_map* map, Lmid_t, uintptr_t* cookie) {
  std::string_view path(map->l_name);

  // NB: There is an inconsistency here with how Glibc understands BINDTO and BINDFROM. If we return
  // 0 in one of these two cases we will detect dlsym bindings but not "regular" symbol resolution.
  // In other words:
  //  - dlsym bindings give callbacks when any of the filters pass (OR), but
  //  - regular symbol resolution only give callbacks when all of the filters pass (AND).
  // Sigh.
  //
  // TODO: Nail this down as a Tier 2 LD_AUDIT issue
  if(std::regex_match(path.begin(), path.end(), libvictim_re)) {
    *cookie = 1;
    return LA_FLG_BINDTO;
  }

  *cookie = 0;
  return LA_FLG_BINDFROM;
}

unsigned int la_objclose(uintptr_t* cookie) {
  // NB: It appears that the cookie for ld.so gets reset back to the original link_map* when dlmopen
  // is called.
  //
  // TODO: Nail this down as a Tier 1 LD_AUDIT issue.
  if(*cookie > 100) {
    std::cerr << "[audit] la_objclose for module " << ((struct link_map*)*cookie)->l_name << " with unset cookie\n";
    return 0;
  }
  if(*cookie > 1)
    victims.at(*cookie - 2) = nullptr;
  return 0;
}

uintptr_t la_symbind64(Elf64_Sym* sym, unsigned int ndx, uintptr_t* ref, uintptr_t* def,
                       unsigned int* flags, const char* symname) {
  if(*def > 0 && std::string_view(symname) == "victim") {
    if(*def == 1) {
      // This libvictim has not yet been wrapped. Do so now.
      std::unique_lock<std::mutex> l(wrap_lock);
      size_t idx;
      for(idx = 0; idx < victims.size() && victims[idx] != nullptr; ++idx);
      if(idx >= victims.size()) {
        std::cerr << "[audit] Ran out of space, leaving victim unwrapped!\n";
        return sym->st_value;
      }
      victims[idx] = (pfn_victim_t)sym->st_value;
      *def = idx + 2;
    }
    return (uintptr_t)wrappers[*def - 2];
  }
  return sym->st_value;
}
