#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#include <cstdlib>
#include <dlfcn.h>
#endif

#include <link.h>

#include "victim.h"

#include <dlfcn.h>
#include <string_view>
#include <mutex>
#include <iostream>
#include <unordered_map>
#include <regex>

unsigned int la_version(unsigned int) {
  return LAV_CURRENT;
}

static std::mutex wrap_lock;
static std::unordered_map<Lmid_t, std::pair<pfn_victim_t, bool>> victims;
static str_t wrapped_victim(const char* msg) {
  // Determine which victim we are using based on the caller's linkage namespace
  const decltype(victims)::mapped_type* victim;
  {
    void* caller = (char*)__builtin_extract_return_addr(__builtin_return_address(0)) - 1;

    Dl_info info;
    struct link_map* caller_map;
    if(dladdr1(caller, &info, (void**)&caller_map, RTLD_DL_LINKMAP) == 0) {
      std::cerr << "[audit] Unable to find caller in wrapper, aborting!\n";
      std::_Exit(5);
    }
    Lmid_t lmid;
    if(dlinfo(caller_map, RTLD_DI_LMID, (void*)&lmid) != 0) {
      std::cerr << "[audit] Unable to determine Lmid from caller link_map, aborting!\n";
      std::_Exit(5);
    }

    std::unique_lock<std::mutex> l(wrap_lock);
    auto it = victims.find(lmid);
    if(it == victims.end()) {
      std::cerr << "[audit] Wrapper called from outside known symbol bindings, aborting!\n";
      std::_Exit(5);
    }
    victim = &it->second;
  }

  // If this linkage namespace has been "tainted", don't wrap
  if(victim->second)
    return victim->first(msg);

  std::string m = msg;
  m += " -> auditor()";
  return victim->first(m.c_str());
}

struct Cookie {
  Cookie(Lmid_t lmid, bool isVictim) : lmid(lmid), isVictim(isVictim) {}
  explicit Cookie(uintptr_t raw) : lmid(raw >> 1), isVictim((raw & 1) != 0) {}
  uintptr_t encode() const {
    return (lmid << 1) | (isVictim ? 1 : 0);
  }

  Lmid_t lmid;
  bool isVictim : 1;
};

unsigned int la_objopen(struct link_map* map, Lmid_t lmid, uintptr_t* cookie) {
  std::string_view path(map->l_name);
  *cookie = Cookie(lmid, false).encode();
  return LA_FLG_BINDFROM | LA_FLG_BINDTO;
}

unsigned int la_objclose(uintptr_t* cookie) {
  // NB: It appears that the cookie for ld.so gets reset back to the original link_map* when dlmopen
  // is called.
  //
  // TODO: Nail this down as a Tier 1 LD_AUDIT issue.
  if(*cookie > 100) {
    std::cerr << "[audit] la_objclose for module " << ((struct link_map*)*cookie)->l_name << " with unset cookie\n";
    return 0;
  }
  Cookie c(*cookie);
  if(c.isVictim) {
    std::unique_lock<std::mutex> l(wrap_lock);
    victims.erase(c.lmid);
  }
  return 0;
}

uintptr_t la_symbind64(Elf64_Sym* sym, unsigned int ndx, uintptr_t* ref, uintptr_t* def,
                       unsigned int* flags, const char* symname) {
  Cookie dc(*def);
  if(std::string_view(symname) == "victim") {
    if(!dc.isVictim) {
      // This is a brand new victim symbol to wrap
      {
        std::unique_lock<std::mutex> l(wrap_lock);
        auto [it, wasNew] = victims.try_emplace(dc.lmid,
            std::make_pair((pfn_victim_t)sym->st_value, false));
        if(!wasNew) {
          std::cerr << "[audit] Multiple victims in a single namespace, not intercepting!\n";
          it->second.second = true;
          return sym->st_value;
        }
      }
      dc.isVictim = true;
      *def = dc.encode();
    }
    return (uintptr_t)&wrapped_victim;
  }
  return sym->st_value;
}
