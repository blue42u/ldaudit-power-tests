#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <link.h>

#include "victim.h"

#include <optional>
#include <string_view>
#include <mutex>
#include <iostream>
#include <array>
#include <regex>

unsigned int la_version(unsigned int) {
  return LAV_CURRENT;
}

// This auditor supports up to 3 implementations for the victim. This value can be increased in
// practice but for compatibility with the test suite we limit it here.
static std::mutex wrap_lock;
static std::array<pfn_victim_t, 3> victims = {nullptr, nullptr, nullptr};

static thread_local std::optional<std::pair<std::string, std::size_t>> recursive;
template<int Idx>
static str_t wrapped_victim(const char* msg) {
  std::string m = msg;
  if(recursive) {
    // Un-do the effects from the previous call in this stack
    auto [prefix, trimsize] = std::move(recursive.value());
    if(std::string_view(m).substr(0, prefix.size()) == prefix) {
      m.erase(trimsize, prefix.size() - trimsize);
    } else {
      std::cerr << "[audit] Unable to undo the effects of an interposed wrapper, result will be off!\n";
    }
  }
  // Save data for the next recursive call, if needed
  size_t trimsize = m.size();
  m += " -> auditor()";
  recursive = {m, trimsize};

  str_t result;
  try {
    result = victims.at(Idx)(m.c_str());
  } catch (...) {
    recursive = std::nullopt;
    throw;
  }
  recursive = std::nullopt;
  return result;
}
static std::array<pfn_victim_t, victims.size()> wrappers = {
  wrapped_victim<0>, wrapped_victim<1>, wrapped_victim<2>
};

unsigned int la_objopen(struct link_map* map, Lmid_t, uintptr_t* cookie) {
  *cookie = 0;
  return LA_FLG_BINDTO | LA_FLG_BINDFROM;
}

unsigned int la_objclose(uintptr_t* cookie) {
  // NB: It appears that the cookie for ld.so gets reset back to the original link_map* when dlmopen
  // is called.
  //
  // TODO: Nail this down as a Tier 1 LD_AUDIT issue.
  if(*cookie > 100) {
    std::cerr << "[audit] la_objclose for module " << ((struct link_map*)*cookie)->l_name << " with unset cookie\n";
    return 0;
  }
  if(*cookie >= 1)
    victims.at(*cookie - 1) = nullptr;
  return 0;
}

uintptr_t la_symbind64(Elf64_Sym* sym, unsigned int ndx, uintptr_t* ref, uintptr_t* def,
                       unsigned int* flags, const char* symname) {
  if(std::string_view(symname) == "victim") {
    if(*def == 0) {
      // This victim has not yet been wrapped. Do so now.
      std::unique_lock<std::mutex> l(wrap_lock);
      size_t idx;
      for(idx = 0; idx < victims.size() && victims[idx] != nullptr; ++idx);
      if(idx >= victims.size()) {
        std::cerr << "[audit] Ran out of space, leaving victim unwrapped!\n";
        return sym->st_value;
      }
      victims[idx] = (pfn_victim_t)sym->st_value;
      *def = idx + 1;
    }
    return (uintptr_t)wrappers[*def - 1];
  }
  return sym->st_value;
}
